export default 
`
type Cardback {
    _id: String!,
    name: String!,
    description: String,
    imgAnimated: String! 
}

type Query {
    cardback(_id: String!): Cardback
    cardbacks: [Cardback]
}

type Mutation {
    addCardback(name: String!,description: String,imgAnimated: String!): Cardback
    editCardback(_id: String!, name: String, description: String, imgAnimated: String): Cardback
    deleteCardback(_id: String!): Cardback
}    
`
;