import {mergeTypes} from "merge-graphql-schemas";


import Cardback from './cardback/';

const types = [Cardback];

export default mergeTypes(types, { all: true });