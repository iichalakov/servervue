import Cardback from '../../../models/Cardback';



export default {
    Query: {
        cardback: (root, args) => {
            return new Promise((resolve, rejects) => {
                Cardback.findOne(args).exec((err,res) => {
                    err ? rejects(err) : resolve(res)
                    // if(err){
                    //     rejects(err)
                    // }
                    // else {
                    //     resolve(res)
                    // }
                })
            })
        },

        cardbacks: () => {
            return new Promise((resolve, rejects) => {
                console.log("comes here")
                Cardback.find({}).populate().exec((err,res) => {
                    err ? rejects(err) : resolve(res);
                })
            })   
        }
    },
    Mutation: {
        addCardback: (root, {name, description, imgAnimated}) => {
            const newCardback = new Cardback({name,description,imgAnimated})
            return new Promise ((resolve,rejects) => {
                newCardback.save((err,res) => {
                    err ?  rejects(err) : resolve(res);
                })
            })
        },

        editCardback: (root, {_id,name, description, imgAnimated}) => {
            return new Promise((resolve,rejects) => {
                Cardback.findOneAndUpdate({_id}, {$set: {name,description,imgAnimated}}).exec((err,res) => {
                    err ? rejects(err) : resolve(res);
                })
            })
        },
    
        deleteCardback: (root,{_id}) => {
            return new Promise((resolve,rejects) => {
                Cardback.findOneAndRemove({_id}).exec((err,res)=>{
                    err ? rejects(err) : resolve(res);
                })
            })
        }//end deleteUser
    }//end Mutation
}//end Query