import {mergeResolvers} from "merge-graphql-schemas";
import Cardback from "./cardback/";

const resolvers = [Cardback];
export default mergeResolvers(resolvers);