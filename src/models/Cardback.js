import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const CardbackShema = new Schema({
    description: {
        type: String,
        required: true
    },
    imgAnimated: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true 
    }
})

const Cardback = mongoose.model("Cardback",CardbackShema);
export default Cardback;